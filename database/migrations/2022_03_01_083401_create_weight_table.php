<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeightTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->comment('用户');
            $table->double('weight')->comment('体重');
            $table->string('year')->default('')->comment('年');
            $table->integer('month')->comment('月');
            $table->integer('day')->comment('日');
            $table->integer('week')->comment('周');
            $table->integer('date')->comment('日期');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weight');
    }
}
