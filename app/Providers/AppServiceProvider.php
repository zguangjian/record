<?php

namespace App\Providers;

use App\Models\Weight;
use App\Observers\WeightObserve;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../helpers.php';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Weight::observe(WeightObserve::class);
        Schema::defaultStringLength(191);
    }
}
