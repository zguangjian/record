<?php

/**
 * Created by PhpStorm.
 * User: zguangjian
 * CreateDate: 2022/3/8 8:17
 * Email: zguangjian@outlook.com
 */

namespace App\Http\Controllers;


use App\Models\Member;
use App\Models\Weight;

class HomeController extends Controller
{
    public function index()
    {
        $user = Member::select(["nickname", 'id'])->pluck('nickname', 'id');
        $record = [];
        $labels = [];
        foreach ($user as $key => $value) {
            $labels[$key] = Weight::whereMemberId($key)->select(["id", "weight", 'date', "member_id"])->orderBy('id', 'asc')->pluck('date');
            $record[$key] = Weight::whereMemberId($key)->select(["id", "weight", 'date', "member_id"])->orderBy('id', 'asc')->get()->toArray();
        }


        return view('index', ['user' => $user, 'record' => $record]);
    }
}
