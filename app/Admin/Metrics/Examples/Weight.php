<?php

/**
 * Created by PhpStorm.
 * User: zguangjian
 * CreateDate: 2022/3/1 10:12
 * Email: zguangjian@outlook.com
 */

namespace App\Admin\Metrics\Examples;

use App\Models\Member;
use Illuminate\Http\Request;
use Dcat\Admin\Widgets\Metrics\Line;

class Weight extends Line
{
    public $memberId = "";
    public $label = "";

    public $title = "";

    public function __construct($title = null, $icon = null)
    {
        $this->memberId = $title;
        return parent::__construct($title, $icon = null);
    }

    /**
     * 初始化卡片内容
     *
     * @return void
     */
    protected function init()
    {
        parent::init();

        $this->title(Member::whereId($this->title)->value('nickname'));
        $this->dropdown([
            '7' => 'Last 7 Days',
            '28' => 'Last 28 Days',
            '30' => 'Last Month',
            '365' => 'Last Year',
        ]);
    }

    /**
     * 处理请求
     *
     * @param Request $request
     *
     * @return mixed|void
     */
    public function handle(Request $request)
    {
        $generator = function ($len, $min = 10, $max = 300) {
            for ($i = 0; $i <= $len; $i++) {
                yield mt_rand($min, $max);
            }
        };
        $lastWeight = \App\Models\Weight::whereMemberId($this->title)->orderByDesc('id')->value('weight');
        dd($lastWeight);
        $this->withContent("$lastWeight KG");
        switch ($request->get('option')) {
            case '365':
                // 图表数据
                $this->withChart(collect($generator(30))->toArray());
                // 直线
                break;
            case '30':
                // 图表数据
                $this->withChart(collect($generator(30))->toArray());
                // 直线
                break;
            case '28':
                // 图表数据
                $this->withChart(collect($generator(28))->toArray());
                // 直线
                break;
            case '7':
            default:
                // 图表数据
                $this->withChart([28, 40, 36, 52, 38, 60, 55,]);
        }
    }

    /**
     * 设置图表数据.
     *
     * @param array $data
     *
     * @return $this
     */
    public function withChart(array $data)
    {
        return $this->chart([
            'series' => [
                [
                    'name' => $this->label,
                    'data' => $data,
                ],
            ],
        ]);
    }

    /**
     * 设置卡片内容.
     *
     * @param string $content
     *
     * @return $this
     */
    public function withContent($content)
    {
        return $this->content(
            <<<HTML
<div class="d-flex justify-content-between align-items-center mt-1" style="margin-bottom: 2px">
    <h2 class="ml-1 font-lg-1">{$content}</h2>
    <span class="mb-0 mr-1 text-80">{$this->label}</span>
</div>
HTML
        );
    }
}
