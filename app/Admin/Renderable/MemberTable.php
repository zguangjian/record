<?php

/**
 * Created by PhpStorm.
 * User: zguangjian
 * CreateDate: 2022/3/1 8:56
 * Email: zguangjian@outlook.com
 */

namespace App\Admin\Renderable;


use App\Admin\Repositories\Member;

use Dcat\Admin\Grid;

class MemberTable extends Grid\LazyRenderable
{
    public function grid(): Grid
    {
        return Grid::make(Member::make(), function (Grid $grid) {
            $grid->column('avatar')->image('',50);
            $grid->column('nickname');
            $grid->column('mobile');
            $grid->quickSearch(['id', 'nickname', 'mobile']);
            $grid->paginate(10);

            $grid->disableActions();
        });
    }
}
