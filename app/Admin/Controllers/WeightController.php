<?php

namespace App\Admin\Controllers;

use App\Admin\Metrics\Examples\NewUsers;
use App\Admin\Renderable\MemberTable;
use App\Admin\Renderable\UserTable;
use App\Admin\Repositories\Member;
use App\Admin\Repositories\Weight;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Layout\Row;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class WeightController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Weight(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('member_id', '用户')->display(function ($value) {
                return \App\Models\Member::whereId($value)->value('nickname');
            });
            $grid->column('weight');
            $grid->column('date')->sortable();
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Weight(), function (Show $show) {
            $show->field('id');
            $show->field('member_id');
            $show->field('weight');
            $show->field('year');
            $show->field('month');
            $show->field('day');
            $show->field('week');
            $show->field('date');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Weight(), function (Form $form) {
            $form->hidden('id');
            if ($form->isCreating()){
                $form->selectTable('member_id', '用户')->from(MemberTable::make())->required();
            }
            $form->currency('weight', '体重(KG)')->required();
            $form->date('date')->default(date('Ymd'));
            $form->hidden('created_at');
            $form->hidden('updated_at');
        });
    }

    public function statistics(Content $content)
    {
        return $content->body(function (Row $row) {
            $row->column(null, '111');
        });
    }
}

