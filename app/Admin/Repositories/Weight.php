<?php

namespace App\Admin\Repositories;

use App\Models\Weight as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Weight extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
