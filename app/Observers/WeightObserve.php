<?php

namespace App\Observers;

use App\Models\Weight;

class WeightObserve
{
    public function set(Weight &$weight)
    {
        $weight->date = date('Ymd', strtotime($weight->date));
        $weight->year = date('Y', strtotime($weight->date));
        $weight->month = date('m', strtotime($weight->date));
        $weight->day = date('d', strtotime($weight->date));
        $weight->week = date('W', strtotime($weight->date));
    }

    /**
     * Handle the Weight "created" event.
     *
     * @param Weight $weight
     * @return void
     */
    public function creating(Weight $weight)
    {
        $this->set($weight);
    }

    /**
     * Handle the Weight "updated" event.
     *
     * @param Weight $weight
     * @return void
     */
    public function updating(Weight $weight)
    {
        $this->set($weight);
    }

    /**
     * Handle the Weight "deleted" event.
     *
     * @param Weight $weight
     * @return void
     */
    public function deleted(Weight $weight)
    {
        //
    }

    /**
     * Handle the Weight "restored" event.
     *
     * @param Weight $weight
     * @return void
     */
    public function restored(Weight $weight)
    {
        //
    }

    /**
     * Handle the Weight "force deleted" event.
     *
     * @param Weight $weight
     * @return void
     */
    public function forceDeleted(Weight $weight)
    {
        //
    }
}
