<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Weight
 *
 * @property false|string date
 * @property int $id
 * @property int $member_id 用户
 * @property float $weight 体重
 * @property string $year 年
 * @property int $month 月
 * @property int $day 日
 * @property int $week 周

 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Weight newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Weight newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Weight query()
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Weight whereYear($value)
 * @mixin \Eloquent
 */
class Weight extends Model
{
    use HasDateTimeFormatter;
    protected $table = 'weight';

    /**
     * @param $value
     * @return false|string
     */
    public function getDateAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

}
