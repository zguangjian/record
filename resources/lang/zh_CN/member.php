<?php 
return [
    'labels' => [
        'Member' => '用户表',
        'member' => '用户表',
    ],
    'fields' => [
        'nickname' => '昵称',
        'mobile' => '手机号',
        'avatar' => '头像',
        'lv' => '等级',
    ],
    'options' => [
    ],
];
