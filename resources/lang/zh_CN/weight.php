<?php 
return [
    'labels' => [
        'Weight' => '体重',
        'weight' => '体重',
    ],
    'fields' => [
        'member_id' => '用户id',
        'weight' => '体重',
        'year' => '年',
        'month' => '月',
        'day' => '日',
        'week' => '周',
        'date' => '日期',
    ],
    'options' => [
    ],
];
