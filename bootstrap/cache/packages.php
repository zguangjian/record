<?php return array (
  'barryvdh/laravel-ide-helper' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\LaravelIdeHelper\\IdeHelperServiceProvider',
    ),
  ),
  'celaraze/dcat-extension-plus' => 
  array (
    'providers' => 
    array (
      0 => 'Celaraze\\DcatPlus\\ServiceProvider',
    ),
  ),
  'dcat-admin/operation-log' => 
  array (
    'providers' => 
    array (
      0 => 'Dcat\\Admin\\OperationLog\\OperationLogServiceProvider',
    ),
  ),
  'dcat/laravel-admin' => 
  array (
    'providers' => 
    array (
      0 => 'Dcat\\Admin\\AdminServiceProvider',
    ),
  ),
  'facade/ignition' => 
  array (
    'providers' => 
    array (
      0 => 'Facade\\Ignition\\IgnitionServiceProvider',
    ),
    'aliases' => 
    array (
      'Flare' => 'Facade\\Ignition\\Facades\\Flare',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'fruitcake/laravel-cors' => 
  array (
    'providers' => 
    array (
      0 => 'Fruitcake\\Cors\\CorsServiceProvider',
    ),
  ),
  'jacobcyl/ali-oss-storage' => 
  array (
    'providers' => 
    array (
      0 => 'Jacobcyl\\AliOSS\\AliOssServiceProvider',
    ),
  ),
  'laravel/sail' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Sail\\SailServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'protoqol/prequel' => 
  array (
    'providers' => 
    array (
      0 => 'Protoqol\\Prequel\\PrequelServiceProvider',
    ),
  ),
  'spatie/eloquent-sortable' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\EloquentSortable\\EloquentSortableServiceProvider',
    ),
  ),
);